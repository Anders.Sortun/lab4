package datastructure;

import cellular.CellState;
// import java.util.List;

public class CellGrid implements IGrid {

    int rows;
    int cols;
    // List<CellState>[][] grid;
    CellState[][] grid;
    // IGrid[][] grid;


    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
        // grid = new List<CellState>[rows][columns];
        this.rows = rows;
        this.cols = columns;
        grid = new CellState[rows][columns];
        for (int i = 0; i < rows; i++){
            for (int j = 0; j < cols; j++){
                grid[i][j] = initialState;
            }
        }
	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return this.rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return this.cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        if (row < 0 || row >= numRows()){throw new IndexOutOfBoundsException("Row out of bounds");}
        if (column < 0 || column >= numColumns()){throw new IndexOutOfBoundsException("Column out of bounds");}
        this.grid[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        if (row < 0 || row >= numRows()){throw new IndexOutOfBoundsException("Row out of bounds");}
        if (column < 0 || column >= numColumns()){throw new IndexOutOfBoundsException("Column out of bounds");}
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        CellGrid Copy = new CellGrid(numRows(),numColumns(),CellState.DEAD);
        for (int i = 0; i < rows; i++){
            for (int j = 0; j < cols; j++){
                Copy.grid[i][j] = this.grid[i][j];
            }
        }
        return Copy;
    }
    
}
